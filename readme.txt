/================================================================================================/
/                                           - Tango Gateway -                                    /
/ Developers : Goetz Andy, Usé Martin 							 	 /
/ Entity : ESRF                      					                         /
/ Scilab Forge Link : http://forge.scilab.org/index.php/p/tango-gateway/                         /
/ actual version : 0.1 										 /
/ License: GPL V3									 	 /
/================================================================================================/

Philosophy : Tango-Controls is a middleware developed by the ESRF. it’s a free open source device-oriented controls toolkit for controlling any kind of hardware or software and building SCADA systems. Some bindings with differents Math Software like MathLab are already developed by those software aren’t free. This gateway is an alternative free-open source binding between our software and a powerfull math software. This gateway will let us develop our Tango Client through a Scilab Client and will have a Mathematical environment to treat date extract from a device server.


V0.1 : 24-01-2014
2 methods :
sci_read_attribute : 
- @param_in : device_adresse (string), attribute_name (string)
- @param_out :attribute_value (double)
- Return the value of the attribute asked by the user with the device address
- Only able to manage scalar attribute
- Use the tango_convert_attribute_to_double and the tango_connexion libs
- Developed in the sci_gateway folder
- How to manage char ?


tango_convert_attribute_to_double
- @param_in : attribute_value (DevAttribute), attribute_type (int)
- @param_out : attribute_value (double)
- Convert the DevAttribute in a Double to be easily managed by Scilab
- Developed in the src folder because it’s a function with param_in with tango type
- Able to manage DevState, DevBoolean, DevShort, DevLong, DevFloat, DevULong, DevUShort, DevDouble.


To Do :
- Import the source of the gateway in the forge.
- Develop a method to return the State of the Device (using the convert_to_double) written in the sci_gateway folder
- Develop a method to return the Status of the Device Device (using the convert_to_double ?) written in the sci_gateway folder
- Develop a method to return the Info of the Device (with a method to convert DevInfo in string written in src folder) writtin in the - sci_gateway folder
- Manage the string type
- Manage all the different types of array
- Manage image and spectrum data type
- Develop UnitTest

For the future :
Be able to develop device server with the scilab core insert inside to be able to call some scilab code inside our device server.
 

