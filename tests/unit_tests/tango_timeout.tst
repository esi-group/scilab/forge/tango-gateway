///Unit tests for the getter and setter of the client side timeout

tango_set_timeout("sys/tg_test/1",3000);
[a,b]=tango_get_timeout("sys/tg_test/1");
assert_checkequal(a , 3000);
assert_checkequal(b , 0);

tango_set_timeout("sys/tg_test/1",5000);
[a,b]=tango_get_timeout("sys/tg_test/1");
assert_checkequal(a , 5000);
assert_checkequal(b , 0);

