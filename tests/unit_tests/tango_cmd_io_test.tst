	//test cmd without input

a = tango_command_inout("sys/tg_test/1","SwitchStates");
assert_checkequal(a, 0);

a = tango_command_inout("sys/tg_test/1","DevVoid")
assert_checkequal(a, 0);

	//test cmd with scalar input

b= 1;

//float
a = tango_command_inout("sys/tg_test/1", "DevFloat", b);
assert_checkequal(a, b);

//bool
a = tango_command_inout("sys/tg_test/1", "DevBoolean", b);
assert_checkequal(a, b);

//double
a = tango_command_inout("sys/tg_test/1", "DevDouble", b);
assert_checkequal(a, b);

//short
a = tango_command_inout("sys/tg_test/1", "DevShort", b);
assert_checkequal(a, b);

//Ushort
a = tango_command_inout("sys/tg_test/1", "DevUShort", b);
assert_checkequal(a, b);

//long
a = tango_command_inout("sys/tg_test/1", "DevLong", b);
assert_checkequal(a, b);

//Ulong
a = tango_command_inout("sys/tg_test/1", "DevULong", b);
assert_checkequal(a, b);

	//test cmd with array input

b= [1:5];

//float
a = tango_command_inout("sys/tg_test/1", "DevVarFloatArray", b);
assert_checkequal(a, b);


//double
a = tango_command_inout("sys/tg_test/1", "DevVarDoubleArray", b);
assert_checkequal(a, b);

//short
a = tango_command_inout("sys/tg_test/1", "DevVarShortArray", b);
assert_checkequal(a, b);

//Ushort
a = tango_command_inout("sys/tg_test/1", "DevVarUShortArray", b);
assert_checkequal(a, b);

//long
a = tango_command_inout("sys/tg_test/1", "DevVarLongArray", b);
assert_checkequal(a, b);

//Ulong
a = tango_command_inout("sys/tg_test/1", "DevVarULongArray", b);
assert_checkequal(a, b);
