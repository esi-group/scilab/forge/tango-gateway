
		//test on simple scalar variable

//bool

a = tango_write_attribute("sys/tg_test/1","boolean_scalar",1)
assert_checkequal(a , 0);

//short

a = tango_write_attribute("sys/tg_test/1","short_scalar",1)
assert_checkequal(a , 0);

//ushort

a = tango_write_attribute("sys/tg_test/1","ushort_scalar",1)
assert_checkequal(a , 0);

//long

a = tango_write_attribute("sys/tg_test/1","long_scalar",1)
assert_checkequal(a , 0);

//ulong

a = tango_write_attribute("sys/tg_test/1","ulong_scalar",1)
assert_checkequal(a , 0);

//float

a = tango_write_attribute("sys/tg_test/1","float_scalar",1)
assert_checkequal(a , 0);

//double

a = tango_write_attribute("sys/tg_test/1","double_scalar",1)
assert_checkequal(a , 0);

		// test on spectrum variable

//bool
a = tango_write_attribute("sys/tg_test/1","boolean_spectrum",rand(1,4))
assert_checkequal(a , 0);

//short

a = tango_write_attribute("sys/tg_test/1","short_spectrum",rand(1,4))
assert_checkequal(a , 0);

//ushort

a = tango_write_attribute("sys/tg_test/1","ushort_spectrum",rand(1,4))
assert_checkequal(a , 0);

//long(bug) - Tangotest need to be fix 

//float

a = tango_write_attribute("sys/tg_test/1","float_spectrum",rand(1,4))
assert_checkequal(a , 0);

//double

a = tango_write_attribute("sys/tg_test/1","double_spectrum",rand(1,4))
assert_checkequal(a , 0);

		// test on image variable


//short

a = tango_write_attribute("sys/tg_test/1","short_image",rand(4,4))
assert_checkequal(a , 0);

//ushort

a = tango_write_attribute("sys/tg_test/1","ushort_image",rand(1,4))
assert_checkequal(a , 0);

//long(bug) - Tangotest need to be fix 

//float

a = tango_write_attribute("sys/tg_test/1","float_image",rand(1,4))
assert_checkequal(a , 0);

//double

a = tango_write_attribute("sys/tg_test/1","double_image",rand(1,4))
assert_checkequal(a , 0);
