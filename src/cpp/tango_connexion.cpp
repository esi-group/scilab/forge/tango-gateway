/*!
 * \file tango_connexion.cpp
 * \brief Create and connect a device proxy to the device server
 * \author muse
 * \copyright 	This file is released under the 3-clause BSD license. See COPYING-BSD.
 */

#include <string>
#include <iostream>
#include <tango.h>
#include <tango_connexion.h>
#include "Scierror.h"

/*! \namespace std
 * standard namespace
 *
 *  \namespace Tango
 *  Tango namespace
 */

using namespace std;
using namespace Tango;


///needed to keep a connexion alive
static DeviceProxy *mydevice = NULL;

/*!
 * \fn DeviceProxy* tango_connexion(char* device_name)
 * \brief function called to create and/or connect a Device Proxy to the Device Server
 * \static DeviceProxy* : keep in memory to check if a connection is already existing
 * \param device_name : name of the Device Server
 * \return DeviceProxy* : keep in memory by the function and return to but usable by the gateway
 */
DeviceProxy* tango_connexion(char* device_name)
{

	string error;
	try
	{

		if (mydevice == NULL || mydevice->dev_name() != device_name)
		{
			///Create the connexion with the Device Server			
			mydevice = new Tango::DeviceProxy(device_name);			
		}
	}
	catch (Tango::DevFailed& df)
	{
		///Catch the error to send it the gateway
		TangoSys_OMemStream o;
		o << "Device " << device_name << " not found" << ends;
		Tango::Except::re_throw_exception(df,
				"TANGO_DeviceNotFound",
				o.str(),
				"tango_connexion");
	}

	///try to ping the device to see if is alive
	try
	{
		mydevice->ping();
		return mydevice;
	}
	catch (Tango::DevFailed& df)
	{
		///Catch the error to send it the gateway
		TangoSys_OMemStream o;
		o << "Device " << device_name << " not answering to ping" << ends;
		Tango::Except::re_throw_exception(df,
				"TANGO_DeviceNotFound",
				o.str(),
				"tango_connexion");
	}

}

