/*!
 * \file tango_connexion.cpp
 * \brief Create and connect a device proxy to the device server
 * \author muse
 * \copyright 	This file is released under the 3-clause BSD license. See COPYING-BSD.
 */

#include <string>
#include <iostream>
#include <tango.h>
#include <tango_device_data.h>

/*! \namespace std
 * standard namespace
 *
 *  \namespace Tango
 *  Tango namespace
 */

using namespace std;
using namespace Tango;



/*!
 * \fn void tango_double_to_devdata(double *double_data, DeviceData &dev_data, string cmd_name, DeviceProxy *dev, int dim_x, int dim_y)
 * \brief function able to convert a double to a Device Data with the good format
 * \param[in] double_data* : pointer on the double needed for the conversion
 * \param[out] dev_data : Device Data created in the gateway waiting to be filled
 * \param[in] cmd_name : name of the command in use
 * \param[in] *dev : device proxy needed for the Tango call
 * \param[in] dim_x : size of the double
 * \param[in] dim_y : size of the double
 */

void tango_double_to_devdata(double *double_data, DeviceData &dev_data, string cmd_name, DeviceProxy *dev, int dim_x, int dim_y)
{
try
{
	///query the device to know what the command is waiting.
	switch(dev->command_query(cmd_name).in_type)
	{
	///select the good type with the good format
	case Tango::DEV_BOOLEAN :
	{
		DevBoolean _data;
		_data = *double_data;
		dev_data << _data;
		break;
	}

	case Tango::DEV_DOUBLE :
	{
		DevDouble _data;
		_data = *double_data;
		dev_data << _data;
		break;
	}

	case Tango::DEV_FLOAT :
	{
		DevFloat _data;
		_data = *double_data;
		dev_data << _data;
		break;
	}

	case Tango::DEV_LONG :
	{
		DevLong _data;
		_data = *double_data;
		dev_data << _data;
		break;
	}

	case Tango::DEV_ULONG :
	{
		DevULong _data;
		_data = *double_data;
		dev_data << _data;
		break;
	}

	case Tango::DEV_SHORT :
	{
		DevShort _data;
		_data = *double_data;
		dev_data << _data;
		break;
	}

	case Tango::DEV_USHORT :
	{
		DevUShort _data;
		_data = *double_data;
		dev_data << _data;
		break;
	}

	case Tango::DEVVAR_BOOLEANARRAY :
	{
		DevVarBooleanArray *_data = new DevVarBooleanArray;
		_data->length(dim_x*dim_y);
		for (int i=0; i < _data->length(); i++)
		{
			(*_data)[i] = (bool)double_data[i];
		}
		dev_data << _data;
		break;
	}

	case Tango::DEVVAR_FLOATARRAY :
	{
		DevVarFloatArray *_data = new DevVarFloatArray;

		_data->length(dim_x*dim_y);
		for (int i=0; i < _data->length(); i++)
		{
			(*_data)[i] = (float)double_data[i];
		}
		dev_data << _data;
		break;
	}

	case Tango::DEVVAR_DOUBLEARRAY :
	{
		DevVarDoubleArray *_data = new DevVarDoubleArray;
		_data->length(dim_x*dim_y);
		for (int i=0; i < _data->length(); i++)
		{
			(*_data)[i] = double_data[i];
		}
		dev_data << _data;
		break;
	}

	case Tango::DEVVAR_LONGARRAY :
	{
		DevVarLongArray *_data = new DevVarLongArray;
		_data->length(dim_x*dim_y);
		for (int i=0; i < _data->length(); i++)
		{
			(*_data)[i] = (int)double_data[i];
		}
		dev_data << _data;
		break;
	}

	case Tango::DEVVAR_ULONGARRAY :
	{
		DevVarULongArray *_data = new DevVarULongArray;
		_data->length(dim_x*dim_y);
		for (int i=0; i < _data->length(); i++)
		{
			(*_data)[i] = (uint)double_data[i];
		}
		dev_data << _data;
		break;
	}

	case Tango::DEVVAR_SHORTARRAY :
	{
		DevVarShortArray *_data = new DevVarShortArray;
		_data->length(dim_x*dim_y);
		for (int i=0; i < _data->length(); i++)
		{
			(*_data)[i] = (short)double_data[i];
		}
		dev_data << _data;
		break;
	}

	case Tango::DEVVAR_USHORTARRAY :
	{
		DevVarUShortArray *_data = new DevVarUShortArray;
		_data->length(dim_x*dim_y);
		for (int i=0; i < _data->length(); i++)
		{
			(*_data)[i] = (ushort)double_data[i];
		}
		dev_data << _data;
		break;
	}

	case Tango::DEV_STATE :
	{
		DevState _data;
		_data << (int)*double_data;
		dev_data << _data;
		break;
	}

	}
}
catch (Tango::DevFailed& df)
{
		TangoSys_OMemStream o;
		o << "Cast a double to DevData failed" << ends;
		Tango::Except::re_throw_exception(df,
				"TANGO_CastFailed",
				o.str(),
				"tango_double_to_devdata");
}
}

/*!
 * \fn void tango_devicedata_to_double(DeviceData &dev_data, double *double_data, int &size_of_data)
 * \brief function able to convert a Device Data to a double with the good format
 * \param[in] dev_data : Device Data created in the gateway waiting to be filled
 * \param[out] double_data : pointer on the Double created in the gateway waiting to be filled
 * \param[in] size_of_data : size of the Device Data
 */

void tango_devicedata_to_double(DeviceData &dev_data, double *double_data, int &size_of_data)
{
try
{
	///query the Device Data type	
	switch(dev_data.get_type())
	{
	///select the good type with the good format
	case Tango::DEV_BOOLEAN :
	{
		DevBoolean _data;
		dev_data >> _data;
		*double_data = _data;
		size_of_data = 1;
		break;
	}

	case Tango::DEV_SHORT :
	{
		DevShort _data;
		dev_data >> _data;
		*double_data = _data;
		size_of_data = 1;
		break;
	}

	case Tango::DEV_USHORT :
	{
		DevUShort _data;
		dev_data >> _data;
		*double_data = _data;
		size_of_data = 1;
		break;
	}

	case Tango::DEV_LONG :
	{
		DevLong _data;
		dev_data >> _data;
		*double_data = _data;
		size_of_data = 1;
		break;
	}

	case Tango::DEV_ULONG :
	{
		DevULong _data;
		dev_data >> _data;
		*double_data = _data;
		size_of_data = 1;
		break;
	}

	case Tango::DEV_FLOAT :
	{
		DevFloat _data;
		dev_data >> _data;
		*double_data = _data;
		size_of_data = 1;
		break;
	}

	case Tango::DEV_DOUBLE :
	{
		dev_data >> *double_data;
		size_of_data = 1;
		break;
	}

	case Tango::DEVVAR_FLOATARRAY :
	{
		const DevVarFloatArray *_data;
		dev_data >> _data;
		for (int i = 0; i < _data->length(); i++)
		{
			double_data[i] = (double)(*_data)[i];
		}
		size_of_data = _data->length();
		break;
	}

	case Tango::DEVVAR_DOUBLEARRAY :
	{
		const DevVarDoubleArray *_data;
		dev_data >> _data;
		for (int i = 0; i < _data->length(); i++)
		{
			double_data[i] = (double)(*_data)[i];
		}
		size_of_data = _data->length();
		break;
	}

	case Tango::DEVVAR_SHORTARRAY :
	{
		const DevVarShortArray *_data;
		dev_data >> _data;
		for (int i = 0; i < _data->length(); i++)
		{
			double_data[i] = (double)(*_data)[i];
		}
		size_of_data = _data->length();
		break;
	}

	case Tango::DEVVAR_USHORTARRAY :
	{
		const DevVarUShortArray *_data;
		dev_data >> _data;
		for (int i = 0; i < _data->length(); i++)
		{
			double_data[i] = (double)(*_data)[i];
		}
		size_of_data = _data->length();
		break;
	}

	case Tango::DEVVAR_LONGARRAY :
	{
		const DevVarLongArray *_data;
		dev_data >> _data;
		for (int i = 0; i < _data->length(); i++)
		{
			double_data[i] = (double)(*_data)[i];
		}
		size_of_data = _data->length();
		break;
	}

	case Tango::DEVVAR_ULONGARRAY :
	{
		const DevVarULongArray *_data;
		dev_data >> _data;
		for (int i = 0; i < _data->length(); i++)
		{
			double_data[i] = (double)(*_data)[i];
		}
		size_of_data = _data->length();
		break;
	}
	}
}
catch (Tango::DevFailed& df)
{
		TangoSys_OMemStream o;
		o << "Cast a DevData to double failed" << ends;
		Tango::Except::re_throw_exception(df,
				"TANGO_CastFailed",
				o.str(),
				"tango_devicedata_to_double");
}
}
