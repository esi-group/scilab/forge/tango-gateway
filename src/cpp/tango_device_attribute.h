/*!
 * \headerfile	""
 * \brief 	header of the tango_device_attribute.cpp file with the functions : tango_convert_attribute_to_double() / tango_convert_attribute_to_string() / tango_convert_double_to_device_attribute()
 * \copyright 	This file is released under the 3-clause BSD license. See COPYING-BSD.
 */
#ifndef __TANGO_DEVICE_ATTRIBUTE_H__
#define __TANGO_DEVICE_ATTRIBUTE_H__


double * tango_convert_attribute_to_double(Tango::DeviceAttribute &, int );
char * tango_convert_attribute_to_string(Tango::DeviceAttribute &, int );
void tango_convert_double_to_device_attribute(double*, string , Tango::DeviceProxy*, int, int, Tango::DeviceAttribute &);




#endif // __TANGO_DEVICE_ATTRIBUTE_H__
