/*!
 * \file 	tango_device_attribute.cpp
 * \brief 	functions needed to manage Device Attribute (extract/insert/format)
 * \author 	muse
 * \copyright 	This file is released under the 3-clause BSD license. See COPYING-BSD.
 */

#include <string>
#include <iostream>
#include <tango.h>
#include <tango_device_attribute.h>
#include "MALLOC.h"

/*! \namespace std
 * standard namespace
 *
 *  \namespace Tango
 *  Tango namespace
 */

using namespace std;
using namespace Tango;


/*!
 * \fn double * tango_convert_attribute_to_double(DeviceAttribute &attr_data, int data_type)
 * \brief function called to convert a Tango Device Attribute to a double
 * \param attr_data : address of the Device Attribute
 * \param data_type :data type of the Device Attribute
 * \return double* : pointer on the double (Device Attribute converted)
 */

double * tango_convert_attribute_to_double(DeviceAttribute &attr_data, int data_type)
{
try
{
	///Get dimension of the variable
	int x=attr_data.get_dim_x();
	int y=NULL;
	if(attr_data.get_data_format()==Tango::IMAGE)
	{
		y = attr_data.get_dim_y();
	}
	else
	{
		///Scalar and Spectrum have y dim as 0
		y = attr_data.get_dim_y()+1;
	}	

	double * array_of_double;
	array_of_double = (double*)MALLOC(sizeof(double)*x*y);

	switch (data_type) {

	case Tango::DEV_BOOLEAN :
	{
		DevVarBooleanArray *_data;
		attr_data >> _data;

		for(int i=0; i<x*y;i++)
		{
			array_of_double[i]=(double)(*_data)[i];

		}
		delete _data;
		break;
	}

	case Tango::DEV_STATE :
	{
		DevState _data;
		attr_data >> _data;
		for(int i=0; i<x*y;i++)
		{
			array_of_double[i]= (double)_data;
		}
		break;
	}


	case Tango::DEV_SHORT :
	{
		DevVarShortArray *_data;
		attr_data >> _data;

		for(int i=0; i<x*y;i++)
		{
			array_of_double[i]=(double)(*_data)[i];
		}
		delete _data;
		break;
	}

	case Tango::DEV_USHORT :
	{
		DevVarUShortArray *_data;
		attr_data >> _data;

		for(int i=0; i<x*y;i++)
		{
			array_of_double[i]=(double)(*_data)[i];
		}
		delete _data;
		break;
	}

	case Tango::DEV_LONG :
	{
		DevVarLongArray *_data;
		attr_data >> _data;

		for(int i=0; i<x*y;i++)
		{
			array_of_double[i]=(double)(*_data)[i];
		}
		delete _data;
		break;
	}

	case Tango::DEV_ULONG :
	{
		DevVarULongArray *_data;
		attr_data >> _data;

		for(int i=0; i<x*y;i++)
		{
			array_of_double[i]=(double)(*_data)[i];
		}
		delete _data;
		break;
	}

	case Tango::DEV_FLOAT :
	{
		DevVarFloatArray *_data;
		attr_data >> _data;

		for(int i=0; i<x*y;i++)
		{
			array_of_double[i]=(double)(*_data)[i];
		}
		delete _data;
		break;
	}

	case Tango::DEV_DOUBLE :
	{
		DevVarDoubleArray *_data;
		attr_data >> _data;

		for(int i=0; i<x*y;i++)
		{
			array_of_double[i]=(double)(*_data)[i];
		}
		delete _data;
		break;
	}
	}
	return array_of_double;
}
catch (Tango::DevFailed& df)
{
		TangoSys_OMemStream o;
		o << "Cast a DevAttr to scalar failed" << ends;
		Tango::Except::re_throw_exception(df,
				"TANGO_CastFailed",
				o.str(),
				"tango_convert_attribute_to_double");
}

}
/*!
 * \fn char * tango_convert_attribute_to_string(DeviceAttribute &attr_data, int data_type)
 * \bug function to convert Device attribute to a string not working at this moment
 */
char * tango_convert_attribute_to_string(DeviceAttribute &attr_data, int data_type)
{
try
{
	///Get dimension of the variable
	int x=attr_data.get_dim_x();
	int y=NULL;
	if(attr_data.get_data_format()==Tango::IMAGE)
	{
		y = attr_data.get_dim_y();
	}
	else
	{
		///Scalar and Spectrum have y dim as 0
		y = attr_data.get_dim_y()+1;
	}
	DevVarStringArray *_data = NULL;
	string * attr_string = NULL;
	attr_string = (string*)MALLOC(sizeof(string)*x*y);
	char * attr_char = (char*)MALLOC(sizeof(char)*x*y);
	attr_data >> _data;


	for(int i=0; i<x*y;i++)
	{
		attr_string[i]=(*_data)[i];

	}
	attr_char=(char*)(*attr_string).c_str();
	free(attr_string);
	return attr_char;
}
catch (Tango::DevFailed& df)
{
		TangoSys_OMemStream o;
		o << "Cast a DevAttr to string failed" << ends;
		Tango::Except::re_throw_exception(df,
				"TANGO_CastFailed",
				o.str(),
				"tango_convert_attribute_to_string");
}	


}

/*!
 * \fn void tango_convert_double_to_device_attribute(double *double_data, string attribute, DeviceProxy *dev, int dim_x, int dim_y, Tango::DeviceAttribute &DevOut)
 * \brief function able to convert a double to a Device Attribute with the good format
 * \param[in] double_data* : pointer on the double needed for the conversion
 * \param[in] attribute : name of the Device Attribute
 * \param[in] *dev : device proxy needed for the Tango call
 * \param[in] dim_x : size of the double
 * \param[in] dim_y : size of the double
 * \param[out] DevOut : Device Attribute created in the gateway waiting to be filled
 */
void tango_convert_double_to_device_attribute(double *double_data, string attribute, DeviceProxy *dev, int dim_x, int dim_y, Tango::DeviceAttribute &DevOut)
{
try
{
	///query data type needed
	switch(dev->attribute_query(attribute).data_type) 
	{
	case Tango::DEV_FLOAT : 
	{
		///query data format
		switch(dev->attribute_query(attribute).data_format)
		{
		case Tango::SCALAR :
		{
			Tango::DeviceAttribute DevTemp(attribute, (float)*double_data);
			DevOut = DevTemp;
			break;
		}
		case Tango::SPECTRUM :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y)); ///use a vector to build a DeviceAttribute
			std::vector<float> v_float(v_double.begin(), v_double.end()); ///cast into the data type needed.
			Tango::DeviceAttribute DevTemp(attribute, v_float); ///Build the DeviceAttribute with his constructor
			DevOut = DevTemp; ///insert the DeviceAttribute built in the ouput
			break;
		}
		case Tango::IMAGE :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y)); //same as Tango::SPECTRUM
			std::vector<float> v_float(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_float, dim_x, dim_y); //Image need 2 dimensions
			DevOut = DevTemp;
			break;
		}
		}
		break;
	}
	case Tango::DEV_BOOLEAN :
	{
		switch(dev->attribute_query(attribute).data_format)
		{
		case Tango::SCALAR :
		{
			Tango::DeviceAttribute DevTemp(attribute, (bool)*double_data);
			DevOut = DevTemp;
			break;
		}
		case Tango::SPECTRUM :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<bool> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp);
			DevOut = DevTemp;
			break;
		}
		case Tango::IMAGE :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<bool> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp, dim_x, dim_y);
			DevOut = DevTemp;
			break;
		}
		}
		break;
	}

	case Tango::DEV_SHORT :
	{
		switch(dev->attribute_query(attribute).data_format)
		{
		case Tango::SCALAR :
		{
			Tango::DeviceAttribute DevTemp(attribute, (short)*double_data);
			DevOut = DevTemp;
			break;
		}
		case Tango::SPECTRUM :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<short> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp);
			DevOut = DevTemp;
			break;
		}
		case Tango::IMAGE :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<short> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp, dim_x, dim_y);
			DevOut = DevTemp;
			break;
		}
		}
		break;
	}

	case Tango::DEV_USHORT :
	{
		switch(dev->attribute_query(attribute).data_format)
		{
		case Tango::SCALAR :
		{
			Tango::DeviceAttribute DevTemp(attribute, (ushort)*double_data);
			DevOut = DevTemp;
			break;
		}
		case Tango::SPECTRUM :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<ushort> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp);
			DevOut = DevTemp;
			break;
		}
		case Tango::IMAGE :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<ushort> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp, dim_x, dim_y);
			DevOut = DevTemp;
			break;
		}
		}
		break;
	}

	case Tango::DEV_DOUBLE :
	{
		switch(dev->attribute_query(attribute).data_format)
		{
		case Tango::SCALAR :
		{
			Tango::DeviceAttribute DevTemp(attribute, *double_data);
			DevOut = DevTemp;
			break;
		}
		case Tango::SPECTRUM :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			Tango::DeviceAttribute DevTemp(attribute, v_double);
			DevOut = DevTemp;
			break;
		}
		case Tango::IMAGE :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			Tango::DeviceAttribute DevTemp(attribute, v_double, dim_x, dim_y);
			DevOut = DevTemp;
			break;
		}
		}
		break;
	}

	case Tango::DEV_LONG :
	{
		switch(dev->attribute_query(attribute).data_format)
		{
		case Tango::SCALAR :
		{
			Tango::DeviceAttribute DevTemp(attribute, (int)*double_data);
			DevOut = DevTemp;
			break;
		}
		case Tango::SPECTRUM :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<int> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp);
			DevOut = DevTemp;
			break;
		}
		case Tango::IMAGE :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<int> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp, dim_x, dim_y);
			DevOut = DevTemp;
			break;
		}
		}
		break;
	}

	case Tango::DEV_ULONG :
	{
		switch(dev->attribute_query(attribute).data_format)
		{
		case Tango::SCALAR :
		{
			Tango::DeviceAttribute DevTemp(attribute, (uint)*double_data);
			DevOut = DevTemp;
			break;
		}
		case Tango::SPECTRUM :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<uint> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp);
			DevOut = DevTemp;
			break;
		}
		case Tango::IMAGE :
		{
			std::vector<double> v_double(double_data, double_data+(dim_x*dim_y));
			std::vector<uint> v_temp(v_double.begin(), v_double.end());
			Tango::DeviceAttribute DevTemp(attribute, v_temp, dim_x, dim_y);
			DevOut = DevTemp;
			break;
		}
		}
		break;
	}

	default :
	{
		cout << "\nA scalar is expected" << endl;
		break;
	}

	}
}
catch (Tango::DevFailed& df)
{
		TangoSys_OMemStream o;
		o << "Cast a DevAttr to scalar failed" << ends;
		Tango::Except::re_throw_exception(df,
				"TANGO_CastFailed",
				o.str(),
				"tango_convert_double_to_device_attribute");
}
}

