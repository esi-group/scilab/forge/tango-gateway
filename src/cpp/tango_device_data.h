/*!
 * \headerfile	""
 * \brief 	header of the tango_device_data.cpp file with the functions : tango_devicedata_to_double() / tango_double_to_devdata()
 * \copyright 	This file is released under the 3-clause BSD license. See COPYING-BSD.
 */
#ifndef __TANGO_TO_DEVDATA_H__
#define __TANGO_TO_DEVDATA_H__


#include "tango.h"

void tango_devicedata_to_double(Tango::DeviceData &, double*, int&);
void tango_double_to_devdata(double*, Tango::DeviceData &, string, Tango::DeviceProxy*, int, int);





#endif // __TANGO_TO_DEVDATA_H__
