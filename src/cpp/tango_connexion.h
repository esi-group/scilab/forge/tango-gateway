/*!
 * \headerfile	""
 * \brief 	header of the tango_connexion.cpp file with the function : tango_connexion()
 * \copyright 	This file is released under the 3-clause BSD license. See COPYING-BSD.
 */


#ifndef __TANGO_CONNEXION_H__
#define __TANGO_CONNEXION_H__
#include <tango.h>

/*!
 * \attention	struct need to be define to avoid static variable
 */

struct _TangoDevice {
	Tango::DeviceProxy *device;
	Tango::AttributeInfoEx attr_info;
};

Tango::DeviceProxy* tango_connexion(char*);

#endif  // __TANGO CONNEXION_H__ 
