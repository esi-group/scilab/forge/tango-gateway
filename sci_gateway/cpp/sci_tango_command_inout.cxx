/*!
 *  \file sci_tango_command_inout.cxx
 *  \brief send a command to a Tango Device Server
 *  \author muse
 *  \version 1.0
 *  \copyright 	This file is released under the 3-clause BSD license. See COPYING-BSD.
 */


#include <string>
#include <iostream> 
#include <tango.h>
#include <vector>
#include <tango_connexion.h>
#include <tango_device_data.h>

/*! \namespace std
 * standard namespace
 *
 *  \namespace Tango
 *  Tango namespace
 */

using namespace std;
using namespace Tango;
/// extern C use to counter C++ mangling
extern "C" 
{

#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include <sciprint.h>

/*!
 * \fn int sci_tango_command_inout(char *fname) DeviceProxy *dev, int dim_x, int dim_y)
 * \brief gateway to send a command to a Device Server
 * \param[in] Device Address : string to give the DS address to the gateway
 * \param[in] Command : string to give the name of the command needed to be sent
 * \param[in] Command Value : sent an optional value with the command
 * \exception Tango::DevFailed& Tango error
 * \returns answer of the command (optional) and the tango_error

 */

int sci_tango_command_inout(char *fname)
{
	SciErr sciErr;

	int m1 = 0, n1 = 0;
	int *piAddressVarOne = NULL;
	char *pStVarOne = NULL;
	int lenStVarOne = 0;
	int iType1 = 0;

	int m2 = 0, n2 = 0;
	int *piAddressVarTwo = NULL;
	char *pStVarTwo = NULL;
	int lenStVarTwo = 0;
	int iType2 = 0;

	int m3 = 0, n3 = 0;
	int *piAddressVarThree = NULL;
	double *pStVarThree = NULL;
	int lenStVarThree = 0;
	int iType3 = 0;

	///Check the number of input argument
	CheckInputArgument(pvApiCtx, 2, 3);

	///Check the number of output argument
	CheckOutputArgument(pvApiCtx, 1,2);

	///get Address of inputs
	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	if(Rhs == 3)
	{
		sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}
	}
	///checks types
	sciErr = getVarType(pvApiCtx, piAddressVarOne, &iType1);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if ( iType1 != sci_strings )
	{
		Scierror(999,"%s: Wrong type for input argument #%d: A string expected.\n",fname,1);
		return 0;
	}

	sciErr = getVarType(pvApiCtx, piAddressVarTwo, &iType2);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if ( iType2 != sci_strings )
	{
		Scierror(999,"%s: Wrong type for input argument #%d: A string expected.\n",fname,2);
		return 0;
	}
	if(Rhs == 3)
	{
		sciErr = getVarType(pvApiCtx, piAddressVarThree, &iType3);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}

		if ( iType3 != sci_matrix )
		{
			Scierror(999,"%s: Wrong type for input argument #%d: A double expected.\n",fname,2);
			return 0;
		}
	}
	///get strings
	///string #1
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	///check size input #1 : Device address
	if ( (m1 != n1) && (n1 != 1) )
	{
		Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,1);
		return 0;
	}
	///alloc string
	pStVarOne = (char*)MALLOC(sizeof(char)*(lenStVarOne + 1));
	///get string One
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne,&m1,&n1,&lenStVarOne,&pStVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	///string #2
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo,&m2,&n2,&lenStVarTwo,&pStVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	///check size input #2 : method
	if ( (m2 != n2) && (n2 != 1) )
	{
		Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,2);
		return 0;
	}
	///alloc string
	pStVarTwo = (char*)MALLOC(sizeof(char)*(lenStVarTwo + 1));
	///get string Two
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarTwo,&m2,&n2,&lenStVarTwo,&pStVarTwo);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	if(Rhs == 3)
	{
		///string #3 : value (optional)
		sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarThree,&m3,&n3,&pStVarThree);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
		}
	}
		/*! \bug string need to be fix
		 check size input #3 
		if ( (m3 != n3) && (n3 != 1) )
	{
		Scierror(999,"%s: Wrong size for input argument #%d: A string expected.\n",fname,3);
		return 0;
	}
	}
		pStVarTwo = (char*)MALLOC(sizeof(char)*(lenStVarThree + 1));
		/get string Three
		sciErr = getMatrixOfString(pvApiCtx, piAddressVarThree,&m3,&n3,&lenStVarThree,&pStVarThree);
		if(sciErr.iErr)
		{
			printError(&sciErr, 0);
			return 0;
	}
*/

	double tango_error = 0;
	bool test_ouput = 0;
	int size_out = NULL;
	///check number of input

	if(Rhs == 3)
	///case #1 : 3 inputs (optional third value)
	{
		try
		{
			DeviceProxy *dev = tango_connexion(pStVarOne); ///connection to the Device Proxy
			DeviceData argin;
			tango_double_to_devdata(pStVarThree, argin, pStVarTwo, dev, m3, n3); ///format the data
			DeviceData argout = dev->command_inout(pStVarTwo, argin); ///send the command
			argout.reset_exceptions(DeviceData::isempty_flag); ///manage empty answer
			if(! argout.is_empty())
			{	
				tango_devicedata_to_double(argout, pStVarThree, size_out);
				test_ouput = argout.is_empty();
			}
			else
			{
				test_ouput = argout.is_empty();
			}
		}
		catch (DevFailed& df)
		{
			///build the error message
			string error = "Tango_Error :\nReason :";
			int error_severity = NULL;
			for (int err = 0; err < df.errors.length(); err++)
			{
				error = error + df.errors[err].reason.in();
				error = error + "\n";
			}

			error = error + "\nDescription : \n";
			for (int err = 0; err < df.errors.length(); err++)
			{
				error = error + df.errors[err].desc.in();
				error = error + "\n";
			}
			error = error + "\nOrigin : \n";
			for (int err = 0; err < df.errors.length(); err++)
			{
				error = error + df.errors[err].origin.in();
				error = error + "\n";
			}

			for (int err = 0; err < df.errors.length(); err++)
			{
				error_severity = df.errors[err].severity;
			}
			error = error +"\nSeverity : ";

			switch(error_severity)
			{
			case Tango::WARN :
				error = error + "WARN";
				break;

			case Tango::ERR :
				error = error +"ERROR";
				break;

			case Tango::PANIC :
				error = error + "PANIC";
				break;
			default :
				error = error + "Unknown severity code";
				break;
			}
			Scierror(2500, error.c_str());
			tango_error = -1;
			free(pStVarOne);
			free(pStVarTwo);
			return 0;
		}
	}
	else
	///case #2 : 2 inputs (no optional value)
	{
		try
		{

			DeviceProxy *dev = tango_connexion(pStVarOne);
			DeviceData argout = dev->command_inout(pStVarTwo);
			argout.reset_exceptions(DeviceData::isempty_flag);

			if(! argout.is_empty())
			{	
				test_ouput = argout.is_empty();
				tango_devicedata_to_double(argout, pStVarThree, size_out);

			}
			else
			{
				test_ouput = argout.is_empty();

			}
		}
		catch (DevFailed& df)
		{
			///build the error message
			string error = "Tango_Error :\nReason :";
			int error_severity = NULL;
			for (int err = 0; err < df.errors.length(); err++)
			{
				error = error + df.errors[err].reason.in();
				error = error + "\n";
			}

			error = error + "\nDescription : \n";
			for (int err = 0; err < df.errors.length(); err++)
			{
				error = error + df.errors[err].desc.in();
				error = error + "\n";
			}
			error = error + "\nOrigin : \n";
			for (int err = 0; err < df.errors.length(); err++)
			{
				error = error + df.errors[err].origin.in();
				error = error + "\n";
			}

			for (int err = 0; err < df.errors.length(); err++)
			{
				error_severity = df.errors[err].severity;
			}
			error = error +"\nSeverity : ";

			switch(error_severity)
			{
			case Tango::WARN :
				error = error + "WARN";
				break;

			case Tango::ERR :
				error = error +"ERROR";
				break;

			case Tango::PANIC :
				error = error + "PANIC";
				break;
			default :
				error = error + "Unknown severity code";
				break;
			}
			tango_error = -1;
			Scierror(2500, error.c_str());
			free(pStVarOne);
			free(pStVarTwo);
			return 0;
		}
	}

	///create result on stack

	free(pStVarOne);
	free(pStVarTwo);
	int m_out = 1;
	int n_out = size_out;
	if(! test_ouput) ///check if there are one or two outputs (value sent by the command)
	{

		createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, m_out, n_out, pStVarThree);
		AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
		createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 2, 1, 1, &tango_error);
		AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 2;
	}
	else
	{
		createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, 1, 1, &tango_error);
		AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
	}


	return 0;
}

}

