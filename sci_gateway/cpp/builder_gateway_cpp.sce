// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_cpp()

string TANGO_ROOT;
TANGO_ROOT=getenv("TANGO_ROOT");
  builder_cpp_path = get_absolute_file_path("builder_gateway_cpp.sce")
  tbx_build_gateway("sci_tango", ..
                    ["tango_read_attribute", "sci_tango_read_attribute"; ..
                     "tango_command_inout", "sci_tango_command_inout"; ..
		     "tango_set_timeout", "sci_tango_set_timeout"; ..
		     "tango_get_timeout", "sci_tango_get_timeout"; ..
		     "tango_write_attribute", "sci_tango_write_attribute"], ..
                    ["sci_tango_read_attribute.cxx", ..
                     "sci_tango_command_inout.cxx", ..
		     "sci_tango_set_timeout.cxx", ..
		     "sci_tango_get_timeout.cxx", ..
		     "sci_tango_write_attribute.cxx"], ..
                    builder_cpp_path,..
                    ["../../src/cpp/libtango_src"],..
                    ["-ltango -llog4tango -lCOS4 -lomniORB4 -lomniDynamic4 -lomnithread -lzmq -ldl -lpthread -lstdc++ "],..
                    ["-g -D_DEBUG -D_REENTRANT -W -I"+TANGO_ROOT+"/include/tango -std=c++0x -Dlinux -I"+builder_cpp_path+"../../src/cpp"]);

//                  ["-ltango -llog4tango -lCOS4 -lomniORB4 -lomniDynamic4 -lomnithread -lzmq -ldl -lpthread -lstdc++ -L"+builder_cpp_path+"../../src/cpp -ltango_convert"],..
//                  ["-g -D_DEBUG -D_REENTRANT -W -I/usr/include/tango -I"+builder_cpp_path+"../../src/cpp -std=c++0x -Dlinux"]);
endfunction

builder_gw_cpp();
clear builder_gw_cpp; // remove builder_gw_cpp on stack
